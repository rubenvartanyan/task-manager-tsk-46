package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.ISessionServiceGraph;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.SessionGraph;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

public class SessionGraphServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionServiceGraph sessionService = new SessionServiceGraph(connectionService, serviceLocator);

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<SessionGraph> sessions = new ArrayList<>();
        final SessionGraph session1 = new SessionGraph();
        final SessionGraph session2 = new SessionGraph();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertTrue(sessionService.findOneById(session1.getId()) == null);
        Assert.assertTrue(sessionService.findOneById(session2.getId()) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() throws NullObjectException {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findOneById(session.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        sessionService.clear();
        final List<SessionGraph> sessions = new ArrayList<>();
        final SessionGraph session1 = new SessionGraph();
        final SessionGraph session2 = new SessionGraph();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() throws NullObjectException {
        final SessionGraph session1 = new SessionGraph();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertNotNull(sessionService.findOneById(sessionId));
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() throws NullObjectException {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionService.findOneById(sessionId) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() throws NullObjectException {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        final String sessionId = session.getId();
        sessionService.removeOneById(sessionId);
        Assert.assertFalse(sessionService.findOneById(sessionId) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() throws NullObjectException {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        sessionService.remove(session);
        Assert.assertNotNull(sessionService.findOneById(session.getId()));
    }

}
