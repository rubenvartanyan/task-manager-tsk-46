package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.dto.ITaskRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.repository.dto.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService extends AbstractService<Task>
        implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @SneakyThrows
    @Override
    public @NotNull Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyIdException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final Task task) {
        if (task == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Task> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entities.forEach(taskRepository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Task entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeOneById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findOneByIdAndUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findOneByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId, @Nullable final Task entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeOneByIdAndUserId(userId, entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeOneByIdAndUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null) throw new IncorrectIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull Task task = taskRepository.findOneByIndex(userId, index);
            if (task == null) throw new NullObjectException();
            taskRepository.removeOneByIdAndUserId(userId, task.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final Task entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final Task entity = findOneByName(userId, name);
        entity.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId, 
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Task entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId, 
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyIdException();
        @NotNull final Task entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Task entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final Task entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Task entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyNameException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}
