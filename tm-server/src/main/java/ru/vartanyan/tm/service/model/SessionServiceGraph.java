package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.ISessionServiceGraph;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.system.*;
import ru.vartanyan.tm.model.SessionGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.repository.model.SessionRepositoryGraph;
import ru.vartanyan.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionServiceGraph extends AbstractServiceGraph<SessionGraph>
        implements ISessionServiceGraph {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionServiceGraph(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final SessionGraph session) {
        if (session == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || password == null) return false;
        final @NotNull UserGraph user = serviceLocator.getUserService().findByLogin(login);
        if (user.getLocked()) throw new UserLockedException();
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull UserGraph user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final SessionGraph session = new SessionGraph();
        session.setUser(user);
        @Nullable final SessionGraph signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            sessionRepository.add(signSession);
            entityManager.getTransaction().commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null) throw new AccessDeniedException();
        if (session.getUser() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionGraph sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
        if (sessionRepository.findOneById(session.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph session,
                              @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (session.getUser().getId() == null) throw new AccessDeniedException();
        validate(session);
        final @NotNull UserGraph user = serviceLocator.getUserService().findOneById(session.getUser().getId());
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionGraph close(@Nullable final SessionGraph session) {
        if (session == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            sessionRepository.removeOneById(session.getId());
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<SessionGraph> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            entities.forEach(sessionRepository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            sessionRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable SessionGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            sessionRepository.removeOneById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionGraph findOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            return sessionRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager);
            sessionRepository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
