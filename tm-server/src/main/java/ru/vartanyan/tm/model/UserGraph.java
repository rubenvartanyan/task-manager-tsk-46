package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_user")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGraph extends AbstractEntityGraph {

    @Column
    @Nullable
    private String login;

    @Column(name = "password_hash")
    @Nullable private String passwordHash;

    @Column
    @Nullable private String email;

    @Column(name = "first_name")
    @Nullable private String firstName;

    @Column(name = "last_name")
    @Nullable private String lastName;

    @Column(name = "middle_name")
    @Nullable private String middleName;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role = Role.USER;

    @Column
    @NotNull private Boolean locked = false;

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjectGraph> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TaskGraph> taskGraphs = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SessionGraph> sessionGraphs = new ArrayList<>();

}
