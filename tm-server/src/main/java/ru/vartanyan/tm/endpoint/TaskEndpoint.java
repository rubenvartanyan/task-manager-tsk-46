package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.ITaskEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="yourRootElementName")

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getTaskServiceDTO().add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getTaskServiceDTO().findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getTaskServiceDTO().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getTaskServiceDTO().findOneByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getTaskServiceDTO().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().remove(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getTaskServiceDTO().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectTaskServiceDTO().bindTaskByProject(session.getUserId(), projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getProjectTaskServiceDTO().findAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectTaskServiceDTO().unbindTaskFromProject(session.getUserId(), taskId);
    }

}
