package ru.vartanyan.tm.exception.system;

public class NullObjectException extends Exception {

    public NullObjectException() {
        super("Error! ProjectGraph is not found...");
    }

}
