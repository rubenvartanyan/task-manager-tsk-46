package ru.vartanyan.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.model.UserGraph;

import java.util.List;

public interface IUserRepositoryGraph extends IRepositoryGraph<UserGraph> {

    void clear();

    @NotNull
    List<UserGraph> findAll();

    @NotNull
    UserGraph findByLogin(@NotNull String login);

    @NotNull
    UserGraph findOneById(@Nullable String id);

    void removeByLogin(@NotNull String login);

    void removeOneById(@Nullable String id);

}