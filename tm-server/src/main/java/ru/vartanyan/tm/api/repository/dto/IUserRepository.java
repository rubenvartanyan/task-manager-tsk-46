package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void clear();

    @NotNull
    List<User> findAll();

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User findOneById(@Nullable String id);

    void removeByLogin(@NotNull String login);

    void removeOneById(@Nullable String id);

}
