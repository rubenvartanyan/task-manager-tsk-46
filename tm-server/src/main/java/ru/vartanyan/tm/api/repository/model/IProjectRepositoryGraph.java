package ru.vartanyan.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.model.ProjectGraph;

import java.util.List;

public interface IProjectRepositoryGraph extends IRepositoryGraph<ProjectGraph> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<ProjectGraph> findAll();

    @NotNull
    List<ProjectGraph> findAllByUserId(@Nullable String userId);

    @NotNull
    ProjectGraph findOneById(@Nullable String id);

    @NotNull
    ProjectGraph findOneByIdAndUserId(
            @Nullable String userId,
            @NotNull String id
    );

    @NotNull
    ProjectGraph findOneByIndex(
            @Nullable String userId,
            @NotNull Integer index
    );

    ProjectGraph findOneByName(
            @Nullable String userId,
            @NotNull String name
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}

