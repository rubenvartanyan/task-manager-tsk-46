package ru.vartanyan.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.model.SessionGraph;

import java.util.List;

public interface ISessionRepositoryGraph extends IRepositoryGraph<SessionGraph> {

    void clear();

    @NotNull
    List<SessionGraph> findAll();

    @NotNull
    SessionGraph findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}
