package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.model.AbstractEntityGraph;

import javax.persistence.TypedQuery;

public interface IRepositoryGraph<E extends AbstractEntityGraph> {

    void add(@NotNull E entity);

    E getSingleResult(@NotNull TypedQuery<E> query);

    void update(E entity);

}
