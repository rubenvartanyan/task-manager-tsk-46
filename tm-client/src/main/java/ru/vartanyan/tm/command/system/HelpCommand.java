package ru.vartanyan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.service.ICommandService;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.exception.system.NullObjectException;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class HelpCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-h";
    }

    @Override
    public @NotNull String name() {
        return "help";
    }

    @Override
    public @Nullable String description() {
        return "Display list of terminal commands";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new NullObjectException();
        System.out.println("[HELP]");
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        final List<String> commands = commandService.getCommandList();
        for (final String command : commands) System.out.println(command.toString());
    }

}
