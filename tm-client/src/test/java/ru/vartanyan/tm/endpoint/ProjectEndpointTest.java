package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;


import java.util.List;

public class ProjectEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private SessionDTO session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
        endpointLocator.getProjectEndpoint().clearProjects(session);
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addProjectTest() {
        final String projectName = "nameTest";
        final String projectDescription = "nameTest";
        endpointLocator.getProjectEndpoint().addProject(projectName, projectDescription, session);
        final ProjectDTO project = endpointLocator.getProjectEndpoint().findProjectByName(projectName, session);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getDescription());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllProjectTest() {
        endpointLocator.getProjectEndpoint().addProject("project1", "description1", session);
        endpointLocator.getProjectEndpoint().addProject("project2", "description2", session);
        endpointLocator.getProjectEndpoint().addProject("project3", "description3", session);
        Assert.assertEquals(3, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectByIdTest() {
        endpointLocator.getProjectEndpoint().clearProjects(session);
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
        endpointLocator.getProjectEndpoint().addProject("AA", "BB", session);
        List<ProjectDTO> projectList = endpointLocator.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(1, projectList.size());
        final ProjectDTO project = projectList.get(0);
        Assert.assertEquals("AA", project.name);
        final ProjectDTO projectFound = endpointLocator.getProjectEndpoint().findProjectById(project.getId(), session);
        Assert.assertEquals("AA", projectFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIndexTest() {
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
        endpointLocator.getProjectEndpoint().addProject("AA", "BB", session);
        final ProjectDTO projectFound = endpointLocator.getProjectEndpoint().findProjectByIndex(0, session);
        Assert.assertEquals("AA", projectFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByNameTest() {
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
        endpointLocator.getProjectEndpoint().addProject("AA", "BB", session);
        final ProjectDTO projectFound = endpointLocator.getProjectEndpoint().findProjectByName("AA", session);
        Assert.assertEquals("AA", projectFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIdTest() {
        endpointLocator.getProjectEndpoint().clearProjects(session);
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
        endpointLocator.getProjectEndpoint().addProject("LLL", "LLL", session);
        List<ProjectDTO> projectList = endpointLocator.getProjectEndpoint().findAllProjects(session);
        final ProjectDTO project = projectList.get(0);
        endpointLocator.getProjectEndpoint().removeProjectById(project.getId(), session);
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectByNameTest() {
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
        endpointLocator.getProjectEndpoint().addProject("AA", "BB", session);
        endpointLocator.getProjectEndpoint().removeProjectByName("AA", session);
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectByIndexTest() {
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
        endpointLocator.getProjectEndpoint().addProject("AA", "BB", session);
        endpointLocator.getProjectEndpoint().removeProjectByIndex(0, session);
        Assert.assertEquals(0, endpointLocator.getProjectEndpoint().findAllProjects(session).size());
    }

}
